import React,{Component} from 'react';
import './AnalogClock.css';

export default class AnalogClock extends Component{
    render() {
        const currentTime = this.props.time;
        const hoursDeg = (currentTime.getHours()/12)*360-180;
        const minutesDeg = (currentTime.getMinutes()/60)*360-180;
        const secondsDeg = parseInt((currentTime.getSeconds()/60)*360)-180;
        const secondsStyle = {
            transform: `rotate(${secondsDeg}deg)`,
            transformOrigin: '0% 0%'
        };
        const minutesStyle = {
            transform: `rotate(${minutesDeg}deg)`,
            transformOrigin: '0% 0%'
        };
        const hoursStyle = {
            transform: `rotate(${hoursDeg}deg)`,
            transformOrigin: '0% 0%'
        };
        return (
            <div className="clock-body">
                <div className="clock-seconds arrow" style={secondsStyle}></div>
                <div className="clock-minutes arrow" style={minutesStyle}></div>
                <div className="clock-hours arrow" style={hoursStyle}></div>
            </div>
        );
    }
}
