import React,{Component} from 'react';
import AnalogClock from "./components/AnalogClock/AnalogClock";

export default class ClockApp extends Component{
    state = {
        time: new Date()
    };
    timer = null;
    componentDidMount() {
        this.timer = setInterval(this.updateTime,1000);
    }

    componentWillUnmount() {
        clearInterval(this.timer);
    }


    updateTime = () =>{
        this.setState({
            time: (new Date())
        });
    };

    render() {
        return (
            <div className="container">
                <AnalogClock time={this.state.time}/>
                <AnalogClock time={this.state.time}/>
            </div>
            );
    }
}
